### Instructions

1. ensure you have access to a local (or browser-based) server
2. clone the repo (or upload to a browser-based environment)
3. `cd` into `spanos-fronted-test-1` (or only upload this dir)
4. run the server

### Design

#### Custom Element

To allow the scripts in `test-1_page.html` access to the imported elements, I opted to append the sourced HTML directly to the body, rather than use a shadow DOM.

I stored attributes (HTML and JSON source paths) along with an "expanded" state as element properties for easy access. I then used a series of methods to fetch the necessary data and populate the DOM, using a custom tag as the root.

#### Importing HTML

When an instance of the element is appended to the DOM (via customElements.define), the source HTML is fetched and loaded using an `XMLHttpRequest`. I chose this over `fetch` as I began development without using a local server, and I had never used `XMLHttpRequest`, so I was interested to see how it works. If the HTML could not be fetched, an error message is appended to the DOM.

In order for the scripts in `test-1_page.html` to run (handling most toggling), it was necessary to clone their attributes and content into new script tags appended to the DOM. To ensure that all scripts had loaded before any JavaScript was run, an iterative check was also necessary, blocking the loading of enclosed code until imported code is accessible.

#### Fetching Data

To prevent blocking, data is only fetched after the template has been added to the DOM. Data fetching errors are caught and displayed to the user within the list "widget" to provide clear and immediate feedback. Fetched data is then passed to constructor functions that create list elements with the shape of history list item `li`s. I abstracted child generation into separate functions to reduce complexity and increase code flexibility.

The meaning and purpose of values like `provider` and `active` were not specified and are somewhat unclear given their absence from the dummy values in the source HTML. I assumed that both were important to present to the user, and that `provider` is a top-level category, while `active` reflects lock state.

#### Zepto Replacement

I referenced online resources as a starting point for a custom class/function pair that could be used to select and manipulate DOM elements, identifying necessary features by reviewing embedded and imported code. In most cases, it sufficed to pass parameters (and operations) to native DOM methods.

The biggest challenge was balancing code reusability with convenience. Libraries like Zepto or jQuery can accept strings or elements and, return or manipulate both individual and collections of elements. While this flexibility makes them powerful in a more general context, recreating it fully seemed out of scope for this task.

#### Functionality

Most required functionality was provided by the imported `utils` and `collapsible` scripts. Provided these were successfully loaded and executed, had access to relevant elements, and were available when called by other scripts, they worked without issue, though their design placed certain constraints on mine.

"Show All" was the only custom functionality required, though it was necessary to design this in a way that did not conflict with "Show/Hide History" functionality. I achieved this using an event listener on the "show-all" `p`, with a callback that applied or removed a custom "hide" class (with an `!important` flag) from all but the first four list items. Leveraging the rules of CSS inheritance, the class hides or shows "spillover" elements without overriding the inline styling used by the `toggle()` method used in a `test-1_page.html` script.

### Dependencies

#### http-server

Any local or browser-based (e.g., Replit) server will do, but CORS handling and other browser-specific features require that the code be hosted on a server to run properly.
