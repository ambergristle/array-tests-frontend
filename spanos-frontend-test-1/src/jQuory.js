// define custom element collection to store and modify selected elements
class ElementCollection extends Array {
  /*
    return closest ancestor matching selector
    @param selector string element selector
    @returns array of elements or null
  */
  closest(selector) {
    // pass selector to element method
    return this.map((el) => el.closest(selector));
  }

  /*
    set event callback for element; set for all children matching
    selector, if provided
    @param e string event name
    @param selector string element selector
    @param callback fn to execute on event
    @returns void
  */
  on(event, selector = undefined, callback) {
    // add event listener to each element passed
    this.forEach((el) => {
      el.addEventListener(event, (e) => {
        // only add if selector matches, if present
        if (selector) {
          if (e.target.matches(selector)) callback(e);
        } else {
          // add to all if no selector
          callback(e);
        }
      });
    });
  }

  /*
    executes callback when document is ready
    @param callback fn
    @returns void
  */
  ready(callback) {
    // check if any elements are done loading
    const isReady = this.some(
      (element) => element.readyState !== null && e.readyState !== "loading"
    );

    // if document is ready, execute callback, else execute on event
    if (isReady) {
      callback();
    } else {
      this.on("DOMContentLoaded", callback);
    }
  }

  /*
    remove element attribute
    @param attribute string attribute name
    @returns void
  */
  removeAttr(attribute) {
    // pass attribute to element method
    this.forEach((el) => el.removeAttribute(attribute));
  }

  /*
    get or set element text content
    indexed into array for return value as quick fix for use case
    @param newTextContent? value to set element textContent to
    @returns element textContent value if !newTextContent
  */
  text(newTextContent) {
    if (newTextContent) {
      // reset textContent if new content provided
      this.forEach((el) => (el.textContent = `${newTextContent}`));
    } else {
      // else return existing
      return this.map((el) => el.textContent)[0];
    }
  }

  /*
    toggle visibility of a list of elements, inverting the "visibility" of
    the first element; display = "none" || null
    @returns void
  */
  toggle() {
    // get display of first element
    const display = this[0].style.display;
    // update display of all elements to "!display"
    this.forEach((el) => {
      el.style.display = display === "none" ? null : "none";
    });
  }

  /*
    add or remove specified class(es) depending on whether they are absent
    or present (respectively)
    @param classes string space-delimited class "list"
    @returns void
   */
  toggleClass(classes) {
    // parse class names
    const classNames = classes.split(" ");
    // update class list for each element
    this.forEach((el) => {
      // iterate through each specified class
      classNames.forEach((className) => {
        // parse element classes
        const elClasses = el.className.split(" ");

        if (elClasses.includes(className)) {
          // if class present, remove
          el.classList.remove(className);
        } else {
          // else add
          el.classList.add(className);
        }
      });
    });
  }
}

/*
  instantiates ElementCollection with selected or passed element
  @param selectorOrElement string selector or dom element
  @returns new ElementCollection
*/
function $(selectorOrElement) {
  if (typeof selectorOrElement === "string") {
    return new ElementCollection(...document.querySelectorAll(arg));
  } else {
    return new ElementCollection(arg);
  }
}
