// define custom html element
class ArrayCreditLock extends HTMLElement {
  constructor() {
    super();

    // get attribute values
    this.src = this.getAttribute("src");
    this.dataSrc = this.getAttribute("dataSrc");

    // create template element
    this._template = document.createElement("template");

    // set expanded state
    this.expanded = false;
  }

  //
  async connectedCallback() {
    try {
      // fetch and store html source code, using cloneNode to make children accessible
      this._template.innerHTML = await this._getSourceHTML(this.src);
      this._htmlContent = this._template.content.cloneNode(true);

      // store references to relevant elements
      this._showAllBtn = this._htmlContent.querySelector(".show-all");
      this._lockWrapper = this._htmlContent.querySelector(".lock-wrapper");
      this._historyList = this._htmlContent.querySelector(
        ".history-title + ul"
      );

      // clear dummy history list elements and add template to dom
      // using a shadow dom would make children inaccessible to external scripts
      this._historyList.replaceChildren();
      this._createTemplate();

      // fetch and store data, create and append new history list elements
      this.data = await this._fetchData();
      this._populateTemplate(this.data);

      // add list item count and onClick handler to "show-all" button
      // bind to class to make props accessible
      this._showAllBtn.textContent = `Show All (${this.data.length})`;
      this._showAllBtn.addEventListener("click", this._toggleExpand.bind(this));
    } catch (err) {
      // display error message if page fails to load
      const errorMessage = document.createElement("p");
      errorMessage.textContent = err.message;
      document.body.prepend(errorMessage);
    }
  }

  /*
    clone script tag using document.append to execute js; if script is
    internal, wait until external scripts (dependencies) have loaded
    @param scriptTag element (script tag)
    returns void
  */
  _cloneScriptTag(scriptTag) {
    // create new script tag and extract key values from existing tag
    const tag = document.createElement("script");
    const src = scriptTag.getAttribute("src");
    const textContent = scriptTag.textContent;

    if (src) {
      // if code is imported, set new tag src and add to document
      tag.setAttribute("src", src);
      document.body.append(tag);
    }

    if (textContent) {
      // if code is enclosed, set new tag textContent
      tag.textContent = textContent;

      // check every 10 ms if external scripts loaded
      (() => {
        const loadingInterval = setInterval(() => {
          if (window.utils) {
            // if loaded, add new tag to body
            document.body.append(tag);
            clearInterval(loadingInterval);
          }
        }, 10);
      })();
    }

    // remove old script tag from template
    this._htmlContent.removeChild(scriptTag);
  }

  /*
    create a history list item using provided values
    @param { provider: string, type: string, date: string, active: bool }
    @returns configured list element
  */
  _createListItem({ provider, type, date, active }) {
    // create a new list item with class "history-list"
    const listElem = document.createElement("li");
    listElem.classList.add("history-list");

    // create an array off children to display passed values
    const children = [
      this._createSpan("provider", provider),
      this._createSpan("type", type),
      this._createSpan("date", this._formatDate(date)),
      this._createLock(active),
    ];

    // append each child element to new list element
    children.forEach((child) => {
      listElem.append(child);
    });

    return listElem;
  }

  /*
    create a new span element with specified class(es) and text
    @param className string class name(s)
    @param textContent string text content
    @returns configured span element
  */
  _createSpan(className, textContent) {
    const span = document.createElement("span");
    span.classList.add(className);
    span.textContent = textContent;
    return span;
  }

  /*
    create a new "lock-wrapper" element clone with specified status
    @param active bool
    @returns configured "lock-wrapper element"
  */
  _createLock(active) {
    const lockWrapper = this._lockWrapper.cloneNode(true);
    const lockedStatus = lockWrapper.querySelector(".lock");
    lockedStatus.textContent = active ? "Unlocked" : "Locked";
    return lockWrapper;
  }

  /*
    prepend imported html to document body, adding script tags with
    document.append so they will execute
    @returns void
  */
  _createTemplate() {
    // get all script tags in source html
    const scripts = this._htmlContent.querySelectorAll("script");

    // display error if html could not be loaded
    if (!scripts) {
      throw new Error("404: page could not be found");
    }

    // clone to new script tag (and remove existing)
    // bind callback to class scope to access _htmlContent
    scripts.forEach(this._cloneScriptTag.bind(this));

    // add html less script tags to body
    document.body.prepend(this._htmlContent);
  }

  /*
    fetch data async and return json; show alert on failure
    @returns array of fetched data
  */
  async _fetchData() {
    try {
      // throw error if no data source specified
      if (!this.dataSrc) throw new Error("data source unspecified");

      // fetch data using source url
      const response = await fetch(this.dataSrc);

      // check fetch success
      if (!response.ok) {
        throw new Error(`${response.status}: data could not be fetched`);
      }

      // return parsed results of successful fetch
      return response.json();
    } catch (err) {
      // display error message in history list
      const errorMessage = document.createElement("li");
      errorMessage.textContent = `${err.message}`;
      this._historyList.append(errorMessage);
    }
  }

  /*
    format date string using client locale data, using 24-hour clock and
    showing time zone
    @param date string
    @returns stringified date in locale format
  */
  _formatDate(date) {
    return new Date(date).toLocaleString({
      hour12: false,
      timeZone: true,
      timeZoneName: "short",
    });
  }

  /*
    xml request html source code, handling error by resolving to null
    @param htmlSource string url of html source
    @returns html source text or null
  */
  async _getSourceHTML(htmlSource) {
    return new Promise((resolve) => {
      // instantiate request class
      const xmlHttpReq = new XMLHttpRequest();

      // open connection with request params
      xmlHttpReq.open("GET", htmlSource);

      // set success and error handler methods
      // resolve to responseText on success, else null
      xmlHttpReq.onload = () => resolve(xmlHttpReq.responseText);
      xmlHttpReq.onerror = () => resolve(null);

      // send request
      xmlHttpReq.send();
    });
  }

  /*
    create and append new "history-list" element for each fetched item
    @param listItems array of list item objects
    @returns void
  */
  _populateTemplate(listItems) {
    listItems.forEach((item, i) => {
      // create new element for each list item
      const el = this._createListItem(item);

      // add hide class for all but first four entries
      if (i > 3) {
        el.classList.add("hide");
      }

      // add to dom
      this._historyList.append(el);
    });
  }

  /*
    toggle expanded history list, showing all items or only first four (default)
    @returns void
  */
  _toggleExpand() {
    // get all history list items
    const historyListItems = [...this._historyList.children];

    // for each, check if in default or expanded group
    historyListItems.forEach((el, i) => {
      if (i > 3) {
        // if hidden by default, check if list is expanded
        if (this.expanded) {
          // hide list + update toggle button text if currently expanded
          el.classList.add("hide");
          this._showAllBtn.textContent = `Show All (${this.data.length})`;
        } else {
          // expand list + update toggle button if currently hidden
          el.classList.remove("hide");
          this._showAllBtn.textContent = "Collapse";
        }
      }
    });

    // invert expanded state
    this.expanded = !this.expanded;
  }
}

// add new custom element of type ArrayCreditLock
customElements.define("array-credit-lock", ArrayCreditLock);
